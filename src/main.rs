use std::fs;
use std::io::Write;
use std::path::Path;

use dlib_face_recognition::{FaceDetectorCnn, FaceDetectorTrait, FaceEncoderNetwork, FaceEncoderTrait, ImageMatrix, LandmarkPredictor, LandmarkPredictorTrait};
use tokio::io::AsyncReadExt;
use v4l::{Device, FourCC};
use v4l::buffer::Type;
use v4l::io::mmap::Stream;
use v4l::io::traits::CaptureStream;
use v4l::video::Capture;

use tefexia_usr::VixensGaze;

fn main() {
    /*generate_reference("/usr/share/face/mmod_human_face_detector.dat",
                       "/usr/share/face/shape_predictor_68_face_landmarks.dat",
                       "/usr/share/face/dlib_face_recognition_resnet_model_v1.dat",
                       "src/reference.jpg", "src/reference.f64"
    );*/
    tokio_main();
}

#[tokio::main]
async fn tokio_main() {
    let mut device = Device::new(0).unwrap();
    let mut format = device.format().unwrap();
    format.fourcc = FourCC::new(&[0, 0, 0, 0]);
    device.set_format(&format).unwrap();

    let gaze = VixensGaze::new().await;

    let mut stdin = tokio::io::stdin();
    loop {
        let char = stdin.read_u8().await.unwrap() as char;
        if char == 'q' {
           break
        } else {
            let mut stream = Stream::with_buffers(&mut device, Type::VideoCapture, 1).unwrap();
            let (buf, metadata) = stream.next().unwrap();
            let part = &buf[0..(metadata.bytesused as usize)];
            let image = image::load_from_memory(part).unwrap().into_rgb8();
            println!("distance: {}", gaze.compare(&image));
        }
    }
}

///
/// Use this for generating reference.f64 yourself
#[allow(dead_code)]
fn generate_reference(
    cnn_path: impl AsRef<Path>,
    landmark_path: impl AsRef<Path>,
    face_encoder_path: impl AsRef<Path>,
    photo_path: impl AsRef<Path>,
    output_path: impl AsRef<Path>
) {
    let cnn = FaceDetectorCnn::open(cnn_path)
        .expect("Error loading Face Detector (CNN).");
    let landmark = LandmarkPredictor::open(landmark_path)
        .expect("Error loading Landmark Predictor.");
    let face_encoder = FaceEncoderNetwork::open(face_encoder_path)
        .expect("Error loading Face Encoder.");

    let image = {
        image::open(photo_path).unwrap().into_rgb8()
    };
    let matrix = ImageMatrix::from_image(&image);
    let faces = cnn.face_locations(&matrix);
    let face = faces.first().unwrap();

    let landmarks = landmark.face_landmarks(&matrix, face);
    let encodings = face_encoder
        .get_face_encodings(&matrix, &[landmarks], 0);
    let encoding = encodings.to_vec().remove(0);

    let values: &[f64] = encoding.as_ref();
    let mut file = fs::File::create(output_path).unwrap();
    for v in values {
        let bytes = v.to_be_bytes();
        file.write_all(&bytes).unwrap()
    }
}