use std::ops::Deref;
use dlib_face_recognition::{FaceDetectorTrait, FaceEncoderTrait, FaceEncoding, ImageMatrix, LandmarkPredictorTrait};
use dlib_face_recognition::FaceDetectorCnn;
use dlib_face_recognition::FaceEncoderNetwork;
use dlib_face_recognition::LandmarkPredictor;
use image::{ImageBuffer, Rgb};

pub struct VixensGaze {
    cnn: FaceDetectorCnn,
    landmark: LandmarkPredictor,
    face_encoder: FaceEncoderNetwork,
    reference: FaceEncoding,
}

impl VixensGaze {
    const REFERENCE_ENCODING_VALUES: &'static [u8] = include_bytes!("reference.f64");

    pub async fn new() -> Self {
        let cnn_job = tokio::spawn(async {
            FaceDetectorCnn::open("/usr/share/face/mmod_human_face_detector.dat")
                .expect("Error loading Face Detector (CNN).")
        });
        let landmark_job = tokio::spawn(async {
            LandmarkPredictor::open("/usr/share/face/shape_predictor_68_face_landmarks.dat")
                .expect("Error loading Landmark Predictor.")
        });
        let face_encoder_job = tokio::spawn(async {
            FaceEncoderNetwork::open("/usr/share/face/dlib_face_recognition_resnet_model_v1.dat")
                .expect("Error loading Face Encoder.")
        }
        );

        let cnn = cnn_job.await.unwrap();
        let landmark = landmark_job.await.unwrap();
        let face_encoder = face_encoder_job.await.unwrap();
        let reference = FaceEncoding::from_vec(
            &Self::REFERENCE_ENCODING_VALUES.chunks_exact(8).map(|v| {
                f64::from_be_bytes(<[u8; 8]>::try_from(v).unwrap())
            }).collect()).unwrap();

        Self {
            cnn,
            landmark,
            face_encoder,
            reference,
        }
    }

    pub fn analyze_single(&self, buffer: &ImageBuffer<Rgb<u8>, impl Deref<Target = [u8]>>) -> FaceEncoding {
        let matrix = ImageMatrix::from_image(buffer);
        let faces = self.cnn.face_locations(&matrix);
        let face = faces.first().unwrap();

        let landmarks = self.landmark.face_landmarks(&matrix, face);
        let encoding = self.face_encoder
            .get_face_encodings(&matrix, &[landmarks], 0);
        encoding.to_vec().remove(0)
    }

    pub fn compare(&self, buffer: &ImageBuffer<Rgb<u8>, impl Deref<Target = [u8]>>) -> f64 {
        let encoding = self.analyze_single(buffer);
        let distance = encoding.distance(&self.reference);
        distance
    }
}

